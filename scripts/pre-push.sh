# abort if any of the following commands fail
set -e

echo
echo "(Pre-Push) => skip with --no-verify option"
echo
# Fail on lint errors
npm run lint
# Fail on failing tests
CI=true npm t
