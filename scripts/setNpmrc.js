if (process.env.NODE_ENV !== 'production') {
  return
}

const fs = require('fs')

fs.writeFileSync('.npmrc', '//registry.npmjs.org/:_authToken=${NPM_TOKEN}')
fs.chmodSync('.npmrc', 0600)
