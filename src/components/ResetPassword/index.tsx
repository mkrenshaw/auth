import React, { FC, lazy, memo, useMemo } from 'react'
import { fontSize, PasswordGuidelines } from '@120wateraudit/envirio-components'
import {
  ButtonsWrapper,
  Form,
  FormButton,
  FormContent,
  FormGroup,
  FormGroupError,
  FormGroupLink,
  FormLogo,
  FormSubtitle,
  FormTextField,
  FormTitle
} from '../Login/styles'
import { preventDefault } from 'src/utils/preventDefault'
import { ReactComponent as KeyIcon } from 'src/assets/Key.svg'
import { Link } from 'react-router-dom'
import { InputOnChangeData } from 'semantic-ui-react/dist/commonjs/elements/Input/Input'
import Popup from 'semantic-ui-react/dist/commonjs/modules/Popup/Popup'
import styled from 'styled-components'

const ResetPasswordSuccess = lazy(() => import('./success'))

const ResetPassword: FC<Props> = ({
  loading,
  password,
  passwordConfirm,
  passwordsMatch,
  passwordValid,
  error,
  onSubmit,
  onInputChanged,
  resetPasswordSuccess
}) => {
  const passwordInputStyle = useMemo(
    () => !passwordValid && password.length > 0 && passwordValidationErrorStyle,
    [passwordValid, password.length]
  )

  const passwordConfirmInputStyle = useMemo(
    () =>
      !passwordsMatch &&
      passwordConfirm.length > 0 &&
      passwordValidationErrorStyle,
    [passwordsMatch, passwordConfirm.length]
  )

  const passwordValidationPopupOpen = useMemo(
    () => !passwordValid && password.length > 0,
    [passwordValid, password.length]
  )

  const passwordConfirmValidationPopupOpen = useMemo(
    () => !passwordsMatch && passwordConfirm.length > 0,
    [passwordsMatch, passwordConfirm.length]
  )

  const resetButtonDisabled = useMemo(
    () => loading || !passwordsMatch || !passwordValid,
    [loading, passwordsMatch, passwordValid]
  )

  return resetPasswordSuccess ? (
    <ResetPasswordSuccess />
  ) : (
    <Form onSubmit={preventDefault}>
      <FormContent>
        <FormLogo>
          <KeyIcon height={'3rem'} />
        </FormLogo>
        <FormTitle>Reset Password</FormTitle>
        <FormGroup>
          <FormSubtitle>Please enter your new password.</FormSubtitle>
        </FormGroup>
        <FormGroup>
          <Popup
            content={<PasswordGuidelines />}
            trigger={
              <TriggerWrapper>
                <FormTextField
                  type="password"
                  name="password"
                  placeholder="Password"
                  value={password}
                  neutral
                  onChange={onInputChanged}
                  textalign="center"
                  fontSize={fontSize.subHeader}
                  style={passwordInputStyle}
                />
              </TriggerWrapper>
            }
            position="top center"
            size="small"
            inverted
            hoverable
            variation="wide"
            on="click"
            open={passwordValidationPopupOpen}
            style={popupStyle}
          />
        </FormGroup>
        <FormGroup>
          <Popup
            content={'Passwords do not match'}
            trigger={
              <TriggerWrapper>
                <FormTextField
                  type="password"
                  name="passwordConfirm"
                  placeholder="Confirm Password"
                  value={passwordConfirm}
                  neutral
                  onChange={onInputChanged}
                  textalign="center"
                  fontSize={fontSize.subHeader}
                  style={passwordConfirmInputStyle}
                />
              </TriggerWrapper>
            }
            position="bottom center"
            size="small"
            inverted
            hoverable
            variation="wide"
            on="click"
            open={passwordConfirmValidationPopupOpen}
            style={popupStyle}
          />
        </FormGroup>
        <ButtonsWrapper>
          <FormButton
            variant="primary"
            fontSize={fontSize.subHeader}
            fullWidth
            disabled={resetButtonDisabled}
            onClick={onSubmit}>
            Reset My Password
          </FormButton>
        </ButtonsWrapper>
        {error && <FormGroupError>{error}</FormGroupError>}
        <FormGroupLink>
          <Link to="/">Back to Login</Link>
        </FormGroupLink>
      </FormContent>
    </Form>
  )
}

interface Props {
  loading?: boolean
  password: string
  passwordConfirm: string
  passwordsMatch: boolean
  passwordValid: boolean
  error?: string | null
  resetPasswordSuccess?: boolean
  onSubmit: () => void
  onInputChanged: (
    {
      currentTarget: { name }
    }: React.FormEvent<HTMLInputElement>,
    { value }: InputOnChangeData
  ) => void
}

const passwordValidationErrorStyle: React.CSSProperties = {
  border: '2px solid red'
}

const popupStyle = {
  color: 'white'
}

const TriggerWrapper = styled.div`
  width: 100%;
`

export default memo(ResetPassword)
