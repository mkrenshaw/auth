import React, { FC, memo } from 'react'
import { Link } from 'react-router-dom'
import { ReactComponent as CheckIcon } from 'src/assets/Check.svg'
import styled from 'styled-components'
import { FormContent, FormGroupLink, FormText } from '../Login/styles'

const ResetPasswordSuccess: FC = () => (
  <Wrapper>
    <FormContent>
      <Logo>
        <CheckIcon height={'3rem'} />
      </Logo>
      <FormText>Your password has been reset!</FormText>
      <FormGroupLink>
        <Link to="/">Back to Login</Link>
      </FormGroupLink>
    </FormContent>
  </Wrapper>
)

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 315px;
`

export const Logo = styled.div`
  text-align: center;
`

export default memo(ResetPasswordSuccess)
