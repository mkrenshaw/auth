import React, { memo, useMemo } from 'react'
import { Link } from 'react-router-dom'
import { isEmail } from '@120wateraudit/envirio-components/dist/utils/validations'
import {
  Form,
  FormGroup,
  ButtonsWrapper,
  FormGroupError,
  FormGroupLink,
  FormContent,
  FormTitle,
  FormSubtitle,
  FormButton,
  FormTextField
} from './styles'
import { InputOnChangeData } from 'semantic-ui-react/dist/commonjs/elements/Input/Input'
import { preventDefault } from 'src/utils/preventDefault'
import { fontSize } from '@120wateraudit/envirio-components/dist/theme'

const Login = ({
  loading,
  error,
  email,
  password,
  onSubmit,
  onInputChanged,
  isValidEmail
}: Props) => {
  const loginDisabled = useMemo(
    () => !isEmail(email) || !isValidEmail || loading || !email || !password,
    [email, isValidEmail, loading, password]
  )

  return (
    <Form onSubmit={preventDefault}>
      <FormContent>
        <FormTitle>Login</FormTitle>
        <FormGroup>
          <FormSubtitle>
            Welcome back! Please login to your account.
          </FormSubtitle>
        </FormGroup>
        <FormGroup>
          <FormTextField
            neutral
            type="email"
            name="email"
            placeholder="Email"
            value={email}
            onChange={onInputChanged}
            textalign="center"
            fontSize={fontSize.subHeader}
          />
        </FormGroup>
        <FormGroup>
          <FormTextField
            neutral
            type="password"
            name="password"
            placeholder="Password"
            value={password}
            onChange={onInputChanged}
            textalign="center"
            fontSize={fontSize.subHeader}
          />
        </FormGroup>
        <ButtonsWrapper>
          <FormButton
            variant="primary"
            disabled={loginDisabled}
            onClick={onSubmit}
            fontSize={fontSize.subHeader}
            fontWeight="0"
            fullWidth>
            {loading ? 'Logging in...' : 'Login'}
          </FormButton>
        </ButtonsWrapper>
        {error && (
          <FormGroupError>
            {error === '403' ? (
              <>
                Your user account does not have the permission needed to access.
                Please contact
                <a
                  target="_blank"
                  href="mailto:feedback@120wateraudit.com"
                  rel="noopener noreferrer">
                  {' '}
                  120Water Support{' '}
                </a>
                for help.
              </>
            ) : (
              error
            )}
          </FormGroupError>
        )}
        <FormGroupLink>
          <Link to="forgot-password">Forgot your password?</Link>
        </FormGroupLink>
      </FormContent>
    </Form>
  )
}

interface Props {
  email: string
  isValidEmail: boolean
  password: string
  loading?: boolean
  error?: string
  onSubmit: () => void
  onInputChanged: (
    {
      currentTarget: { name }
    }: React.FormEvent<HTMLInputElement>,
    { value }: InputOnChangeData
  ) => void
}

export default memo(Login)
