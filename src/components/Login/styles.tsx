import styled from 'styled-components'
import {
  Button,
  colors,
  fontSize,
  lineHeight,
  spacing,
  TextField
} from '@120wateraudit/envirio-components'

export const Form = styled.form`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  max-width: 315px;
`

export const FormContent = styled.div`
  align-self: center;
  width: 300px;
`

export const FormHeader = styled.div`
  background: #ffffff;
  padding-bottom: ${spacing.huge};
  text-align: center;
  flex-shrink: 0;
`

export const FormTitle = styled.h2`
  text-align: center;
  margin-top: 0;
`

export const FormSubtitle = styled.p`
  text-align: center;
  color: ${colors.black50};
  margin-bottom: 3rem;
`

export const FormGroup = styled.div`
  margin-bottom: 1.33rem;
  width: 100%;
  display: flex;
  justify-content: center;
  flex-shrink: 0;
`

export const FormGroupLink = styled(FormGroup)`
  color: ${colors.black50};
  font-size: ${fontSize.caption};
  line-height: ${lineHeight.body};
  a,
  a:hover,
  a:active,
  a:visited,
  a:link {
    color: ${colors.black50};
  }

  a:hover {
    color: rgba(0, 181, 225, 1);
  }

  margin-bottom: 0;
`

export const FormGroupError = styled(FormGroup)`
  color: ${colors.error};
  display: inline-block;
  text-align: center;
  font-size: ${fontSize.caption};
  line-height: ${lineHeight.caption};
`

export const ButtonsWrapper = styled.div`
  width: 100%;
  padding-bottom: ${spacing.tiny};
  flex-shrink: 0;
`

export const FormText = styled.div`
  font-size: ${fontSize.subHeader};
  font-weight: 700;
  color: ${colors.black};
  text-align: center;
  line-height: ${lineHeight.subHeader};
  padding-top: ${spacing.tiny};
  padding-bottom: ${spacing.tiny};
`

export const FormTextDetail = styled.div`
  font-size: ${fontSize.body};
  line-height: ${lineHeight.body};
  font-weight: 300;
  color: ${colors.black50};
  font-weight: 300;
  text-align: center;
  padding-bottom: ${spacing.tiny};
`

export const FormLogo = styled.div`
  margin-bottom: 1rem;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const FormButton = styled(Button)`
  height: 36px;
  border-radius: 4px;
  font-size: 1.2rem;
  padding: 0;
  font-weight: bold;
`

export const FormTextField = styled(TextField)`
  width: 100%;
  & input {
    border-radius: 4px !important;
    font-size: 1.2rem !important;
  }
`
