import React, { memo, ReactNode } from 'react'
import styled from 'styled-components'
import BaseFooter from 'src/components/Footer'
import { ReactComponent as LoginLogo } from 'src/assets/Logo.svg'
import StreetsImage from 'src/assets/Streets.jpg'

const UnauthenticatedApp: React.FC<Props> = ({ children }) => (
  <Wrapper>
    <Splash />
    <Content>
      <Logo />
      <FormWrapper>{children}</FormWrapper>
      <Footer />
    </Content>
  </Wrapper>
)

interface Props {
  children: ReactNode
}

const Wrapper = styled.div`
  height: 100vh;
  width: 100vw;
  display: grid;
  grid-template-columns: 60vw 40vw;
  grid-template-rows: 1fr;
  grid-template-areas: 'Image Form';
`

const Splash = styled.div`
  display: flex;
  width: 100%;
  background: url(${StreetsImage}) no-repeat center center fixed;
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;

  @media screen and (max-width: 720px) {
    display: none;
  }
`

const Content = styled.div`
  width: 100%;
  background: #ffffff;
  display: grid;
  grid-template-columns: 1fr;
  grid-template-rows: 8rem 1fr auto;
  grid-template-areas:
    'Logo'
    'Form'
    'Footer';

  > * {
    place-self: center;
  }
`

const Logo = memo(
  styled(LoginLogo)`
    background: #ffffff;
    width: 18.5rem;
    height: 6rem;
    margin-top: 2rem;
  `
)

const FormWrapper = styled.div`
  display: flex;

  @media screen and(min-width: 720px) {
    width: 45%;
  }
`

const Footer = styled(BaseFooter)`
  position: absolute;
  bottom: 0;
`

export default memo(UnauthenticatedApp)
