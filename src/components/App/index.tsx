import React, { ReactNode, FC, memo } from 'react'
import styled from 'styled-components'
import Footer from '../Footer'
import { Container } from 'semantic-ui-react'
import { useAuth } from 'src/hooks'
import AppProviders from 'src/providers/AppProviders'
import { User } from '@120wateraudit/envirio-components/dist/models/models/Core/User'

const App: FC<Props> = ({ children }) => {
  const { data } = useAuth()

  return (
    <AppProviders user={data?.user as User}>
      <Wrapper>
        <Container>
          <ChildrenWrapper>
            {children}
            <Footer />
          </ChildrenWrapper>
        </Container>
      </Wrapper>
    </AppProviders>
  )
}

interface Props {
  children: ReactNode
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex: 1;
  min-height: 100vh;
`

const ChildrenWrapper = styled.div`
  background-color: transparent;
`

export default memo(App)
