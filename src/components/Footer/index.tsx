import React, { memo, FC } from 'react'
import styled from 'styled-components'
import { ReactComponent as FooterIcon } from 'src/assets/Footer.svg'
import {
  colors,
  fontSize,
  lineHeight,
  spacing
} from '@120wateraudit/envirio-components/dist/theme'

const Footer: FC = () => (
  <FooterWrapper>
    <FooterIcon height={'1.333rem'} opacity={0.25} />
    <ContentWrapper>
      <span>
        © {new Date().getFullYear()} 120Water™ •{' '}
        <a
          href="https://120water.com/terms-of-use"
          target="_blank"
          rel="noreferrer noopener">
          Terms and Conditions
        </a>{' '}
        •{' '}
        <a
          href="https://120water.com/privacy-policy"
          target="_blank"
          rel="noreferrer noopener">
          Privacy Policy
        </a>
      </span>
    </ContentWrapper>
  </FooterWrapper>
)

const FooterWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${colors.black25};
  padding: ${spacing.tiny} 0;
`

const ContentWrapper = styled.div`
  display: flex;
  justify-content: center;
  text-align: center;
  flex-shrink: 0;
  font-size: ${fontSize.small};
  line-height: ${lineHeight.small};
  margin-top: 0.6767rem;

  & a {
    color: inherit;
    &:hover {
      color: rgba(0, 181, 225, 1);
    }
  }
`

export default memo(Footer)
