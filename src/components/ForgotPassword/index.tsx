import React, { lazy, FC, memo } from 'react'
import { Link } from 'react-router-dom'
import {
  FormGroup,
  ButtonsWrapper,
  FormGroupError,
  FormGroupLink,
  FormLogo,
  FormSubtitle,
  FormTitle,
  FormButton,
  FormTextField,
  FormContent,
  Form
} from '../Login/styles'
import { InputOnChangeData } from 'semantic-ui-react/dist/commonjs/elements/Input/Input'
import { ReactComponent as LockIcon } from 'src/assets/Lock.svg'
import { preventDefault } from 'src/utils/preventDefault'
import { fontSize } from '@120wateraudit/envirio-components/dist/theme'

const ForgotPasswordSuccess = lazy(() => import('./success'))

const ForgotPassword: FC<Props> = ({
  isLoading,
  isValidEmail,
  error,
  email,
  onSubmit,
  onInputChanged,
  onBackToLogin,
  forgotPasswordSuccess
}) =>
  forgotPasswordSuccess ? (
    <ForgotPasswordSuccess email={email} onBackToLogin={onBackToLogin} />
  ) : (
    <Form onSubmit={preventDefault}>
      <FormContent>
        <FormLogo>
          <LockIcon height={'3rem'} />
        </FormLogo>
        <FormTitle>Forgot Password</FormTitle>
        <FormGroup>
          <FormSubtitle>
            Enter your email below and we’ll send you instructions for how to
            reset your password.
          </FormSubtitle>
        </FormGroup>
        <FormGroup>
          <FormTextField
            neutral
            type="email"
            name="email"
            placeholder="Email"
            value={email}
            onChange={onInputChanged}
            textalign="center"
            fontSize={fontSize.subHeader}
          />
        </FormGroup>
        <ButtonsWrapper>
          <FormButton
            variant="primary"
            fontSize={fontSize.subHeader}
            fullWidth
            disabled={!isValidEmail || isLoading}
            onClick={onSubmit}>
            Send Reset Link
          </FormButton>
        </ButtonsWrapper>
        {error && <FormGroupError>{error}</FormGroupError>}
        <FormGroupLink>
          <Link to="/">Back to Login</Link>
        </FormGroupLink>
      </FormContent>
    </Form>
  )

interface Props {
  email: string
  isValidEmail: boolean
  forgotPasswordSuccess: boolean
  error?: string
  isLoading?: boolean
  onSubmit: () => void
  onInputChanged: (
    {
      currentTarget: { name }
    }: React.FormEvent<HTMLInputElement>,
    { value }: InputOnChangeData
  ) => void
  onBackToLogin: () => void
}

export default memo(ForgotPassword)
