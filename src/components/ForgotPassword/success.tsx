import React, { FC, memo } from 'react'
import { Link } from 'react-router-dom'
import {
  FormGroup,
  FormGroupLink,
  FormLogo,
  FormTitle,
  FormSubtitle,
  FormContent
} from '../Login/styles'
import { ReactComponent as EmailIcon } from 'src/assets/Email.svg'
import styled from 'styled-components'
import { colors } from '@120wateraudit/envirio-components/dist/theme'

const ForgotPasswordSuccess: FC<Props> = ({ email, onBackToLogin }) => (
  <FormContent>
    <FormLogo>
      <EmailIcon height={'3rem'} />
    </FormLogo>
    <FormTitle>Please check your email!</FormTitle>
    <FormGroup>
      <FormSubtitle>
        We’ve sent password reset instructions to{' '}
        <EmailWrapper>{email}</EmailWrapper>
      </FormSubtitle>
    </FormGroup>
    <FormGroupLink>
      <Link to="/" onClick={onBackToLogin}>
        Back to Login
      </Link>
    </FormGroupLink>
  </FormContent>
)

interface Props {
  email: string
  onBackToLogin: () => void
}

const EmailWrapper = styled.span`
  color: ${colors.primary};
`

export default memo(ForgotPasswordSuccess)
