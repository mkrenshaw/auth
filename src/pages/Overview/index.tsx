import React, { FC, memo } from 'react'
import { useUser } from 'src/hooks'

const Overview: FC = () => {
  const { firstName } = useUser()

  return <h1>Hello, {firstName}</h1>
}

export default memo(Overview)
