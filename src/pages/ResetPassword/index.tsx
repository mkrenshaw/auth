import React, { memo } from 'react'
import ResetPassword from 'src/containers/ResetPassword'

const ResetPasswordPage = () => <ResetPassword />

export default memo(ResetPasswordPage)
