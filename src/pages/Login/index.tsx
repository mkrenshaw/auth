import React, { lazy, memo } from 'react'

const LoginContainer = lazy(() => import('src/containers/Login'))

const LoginPage = () => <LoginContainer />

export default memo(LoginPage)
