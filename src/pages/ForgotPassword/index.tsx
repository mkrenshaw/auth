import React, { memo } from 'react'
import ForgotPassword from 'src/containers/ForgotPassword'

const ForgotPasswordPage = () => <ForgotPassword />

export default memo(ForgotPasswordPage)
