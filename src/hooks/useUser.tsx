import { useContext } from 'react'
import { UserContext } from 'src/contexts'

/**
 * `useUser` - grabs the user object as well as if the user is a 120Employee
 *
 */
export const useUser = () => useContext(UserContext)
