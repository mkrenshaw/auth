import { useEffect, useReducer, useCallback, useRef } from 'react'
import { useAbort } from './useAbort'
import { API } from 'src/API'
import { getItem, LocalStorageItem } from 'src/utils/localStorage'
import { AxiosError, AxiosRequestConfig } from 'axios'

/**
 * `useFetch` - axios request
 *
 * Note: Accepts type generic
 */
export function useFetch<T>({
  url,
  lazy = false,
  options: baseOptions,
  onCompleted: baseOnCompleted,
  onError: baseOnError,
  onLoading: baseOnLoading
}: UseFetchParams<T>) {
  const { abort, hasAborted } = useAbort()
  const [state, dispatch] = useReducer(fetchReducer<T>(), {
    loading: undefined,
    data: undefined,
    error: undefined
  })
  const options = useRef(baseOptions)
  const onCompleted = useRef(baseOnCompleted)
  const onError = useRef(baseOnError)
  const onLoading = useRef(baseOnLoading)

  const fetchData = useCallback(
    async (requestOptions?: AxiosRequestConfig) => {
      dispatch({ type: DispatchType.LOADING })
      let data: T
      try {
        const response = await API(
          url,
          appendOptions({ ...requestOptions, ...options.current })
        )

        data = response.data as T

        if (!hasAborted) {
          dispatch({ type: DispatchType.RESPONSE, payload: data })
        }
      } catch (error) {
        if (!hasAborted) {
          dispatch({
            type: DispatchType.ERROR,
            payload: error
          })
        }

        throw error
      } finally {
        if (!hasAborted) {
          dispatch({ type: DispatchType.LOADING })
        }
      }

      return data
    },
    [url, hasAborted]
  )

  useEffect(() => {
    if (!lazy) {
      fetchData()
    }
  }, [fetchData, lazy])

  useEffect(() => {
    if (onLoading.current) {
      onLoading.current(state.loading)
    }
  }, [state.loading])

  useEffect(() => {
    if (state.error && onError.current) {
      onError.current(state.error)
    }
  }, [state.error])

  useEffect(() => {
    if (state.data && onCompleted.current) {
      onCompleted.current(state.data)
    }
  }, [state.data])

  useEffect(() => {
    return () => {
      if (!hasAborted) {
        abort()
      }
    }
  }, [abort, hasAborted])

  return { ...state, abort, hasAborted, fetchData } as UseFetchReturnType<T>
}

const fetchReducer = <T>() => (
  state: State<T>,
  action: Actions<T>
): State<T> => {
  if (action.type === DispatchType.RESPONSE) {
    return {
      ...state,
      data: action.payload
    }
  }

  if (action.type === DispatchType.ERROR) {
    return {
      ...state,
      error: action.payload
    }
  }

  if (action.type === DispatchType.LOADING) {
    return {
      ...state,
      loading: !state.loading
    }
  }

  return state
}

const appendOptions = (options?: AxiosRequestConfig) => ({
  ...options,
  headers: {
    ...(options?.headers && { ...options.headers }),
    Authorization: `Bearer ${getItem(LocalStorageItem.TOKEN)}`
  }
})

interface State<T> {
  loading?: boolean
  data?: T
  error?: AxiosError
}

interface UseFetchParams<T> {
  /** Endpoint excluding `baseURL` */
  url: string
  /** Set to true if you want to manually call `fetchData` function */
  lazy?: boolean
  /** `AxiosRequestConfig` options */
  options?: AxiosRequestConfig
  /** Callback with data from the completed request  */
  onCompleted?: (data: T) => void
  /** Callback with error (AxiosError) from request */
  onError?: (error: AxiosError) => void
  /** Callback with loading boolean */
  onLoading?: (loading?: boolean) => void
}

interface UseFetchReturnType<T> {
  loading: boolean
  data?: T
  error?: Error
  abort: () => void
  hasAborted: boolean
  fetchData: (
    requestOptions?: AxiosRequestConfig | undefined
  ) => Promise<T | undefined>
}

enum DispatchType {
  RESPONSE,
  ERROR,
  LOADING
}

interface ResponseAction<T> {
  type: DispatchType.RESPONSE
  payload: T
}

interface ErrorAction {
  type: DispatchType.ERROR
  payload: AxiosError
}

interface LoadingAction {
  type: DispatchType.LOADING
}

type Actions<T> = ResponseAction<T> | ErrorAction | LoadingAction
