import { useCallback, useState } from 'react'

export const useToggle = (defaultValue = false) => {
  const [enabled, setEnabled] = useState<boolean>(defaultValue)

  const toggle = useCallback(() => {
    setEnabled(p => !p)
  }, [])

  return {
    enabled,
    toggle
  }
}
