import React from 'react'
import { Route, RouteProps } from 'react-router-dom'

const createRoutes = (routes: RouteProps[]): JSX.Element[] =>
  routes.map(({ path, ...rest }, i) => (
    <Route key={`route-${path}-${i}`} path={path} {...rest} />
  ))

export default createRoutes
