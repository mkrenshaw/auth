export const preventDefault = (e: React.FormEvent<HTMLFormElement>) =>
  e.preventDefault()
