export const setItem = <T>(name: LocalStorageItem, data: T): void =>
  window.localStorage.setItem(
    name,
    typeof data === 'string' ? data : JSON.stringify(data)
  )

export const getItem = (name: LocalStorageItem): string | null =>
  window.localStorage.getItem(name)

export const clearItems = (): void => window.localStorage.clear()

export enum LocalStorageItem {
  TOKEN = 'wastewater-token',
  USER = 'wastewater-user'
}
