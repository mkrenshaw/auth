export const validatePassword = (password: string) =>
  /^(?=.*\d)(?=.*[A-Z])(?=.*[@%&~!?#$^+=-])(.{8,15})$/.test(password)
