import axios from 'axios'

// Use proxy for local dev, use ENV var from build otherwise
export const baseURL = process.env.REACT_APP_API_ENDPOINT || ''

// API
export const API = axios.create()
API.defaults.baseURL = baseURL
API.defaults.headers.Accept = 'application/json'
API.defaults.headers['Content-Type'] = 'application/json'

// AUTH
export const login = '/auth/token/pws'
export const validateToken = '/auth/validate'
export const forgotPassword = '/accountmgmt/rest/password-reset'
export const resetPassword = '/accountmgmt/rest/password-reset/reset'
export const verifyResetPassword = '/accountmgmt/rest/password-reset/verify'

// USERS
export const getCurrentUser = '/accountmgmt/rest/currentuser/@me'
