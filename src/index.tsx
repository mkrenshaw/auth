import React, { FC } from 'react'
import ReactDOM from 'react-dom'
import GlobalProviders from './providers/GlobalProviders'
import Router from './router'
import { __DEV__ } from './constants'
import { register, unregister } from './serviceWorker'

import './global.css'

const App: FC = () => (
  <GlobalProviders>
    <Router />
  </GlobalProviders>
)

ReactDOM.render(<App />, document.getElementById('root'))

__DEV__ ? unregister() : register()
