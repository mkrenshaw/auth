import { createContext } from 'react'
import { User } from '@120wateraudit/envirio-components/dist/models'

export const UserContext = createContext<User>({} as User)
