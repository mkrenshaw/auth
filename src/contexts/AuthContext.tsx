import { createContext } from 'react'
import { User } from '@120wateraudit/envirio-components/dist/models'

export const AuthContext = createContext({} as AuthContextOptions)

interface AuthContextOptions {
  logout: () => void
  login: ({
    username,
    password
  }: {
    username: string
    password: string
  }) => Promise<void>
  loginLoading?: boolean
  authenticated: boolean
  authLoading?: boolean
  error?: string
  data?: {
    user: User
    token: string
  }
}
