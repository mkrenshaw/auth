import React, { FC, useState, useCallback, memo, useEffect } from 'react'
import Login from 'src/components/Login'
import { InputOnChangeData } from 'semantic-ui-react/dist/commonjs/elements/Input/Input'
import { useAuth } from 'src/hooks'
import { isEmail } from '@120wateraudit/envirio-components/dist/utils/validations'

const LoginContainer: FC = () => {
  const { login, loginLoading, error: authError } = useAuth()
  const [error, setError] = useState<string | undefined>(undefined)
  const [{ email, password, isValidEmail }, setState] = useState({
    email: '',
    password: '',
    isValidEmail: false
  })

  const onInputChanged = useCallback(
    (
      { currentTarget: { name } }: React.FormEvent<HTMLInputElement>,
      { value }: InputOnChangeData
    ) => {
      if (name === 'email' || name === 'password') {
        setState(prev => ({
          ...prev,
          [name]: value,
          ...(name === 'email' && {
            isValidEmail: isEmail(value)
          })
        }))
      }
    },
    []
  )

  const onLoginSubmitted = useCallback(() => {
    login({ username: email, password })
  }, [login, email, password])

  useEffect(() => {
    if (authError) {
      setError(authError)
      return
    }

    setError(undefined)
  }, [authError])

  return (
    <Login
      loading={loginLoading}
      error={error}
      email={email}
      isValidEmail={isValidEmail}
      password={password}
      onSubmit={onLoginSubmitted}
      onInputChanged={onInputChanged}
    />
  )
}

export default memo(LoginContainer)
