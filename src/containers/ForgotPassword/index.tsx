import React, { FC, useState, useCallback, memo, useEffect } from 'react'
import { InputOnChangeData } from 'semantic-ui-react/dist/commonjs/elements/Input/Input'
import { useToggle, useFetch } from 'src/hooks'
import { isEmail } from '@120wateraudit/envirio-components/dist/utils/validations'
import { forgotPassword as forgotPasswordURL } from 'src/API'
import ForgotPassword from 'src/components/ForgotPassword'

const ForgotPasswordContainer: FC = () => {
  const {
    toggle: toggleForgotPasswordSuccess,
    enabled: forgotPasswordSuccess
  } = useToggle()
  const [error, setError] = useState<string | undefined>(undefined)
  const [{ email, isValidEmail }, setState] = useState({
    email: '',
    isValidEmail: false
  })

  const { fetchData: forgotPassword, loading } = useFetch<boolean>({
    url: forgotPasswordURL,
    lazy: true,
    options: {
      method: 'POST'
    },
    onCompleted: toggleForgotPasswordSuccess,
    onError: e => setError(e.message)
  })

  const onInputChanged = useCallback(
    (
      { currentTarget: { name } }: React.FormEvent<HTMLInputElement>,
      { value }: InputOnChangeData
    ) => {
      setState(prev => ({
        ...prev,
        [name]: value,
        isValidEmail: isEmail(value)
      }))
    },
    []
  )

  const onForgotPasswordSubmit = useCallback(() => {
    forgotPassword({ data: { email } })
  }, [email, forgotPassword])

  const resetForgotPassword = useCallback(() => {
    if (forgotPasswordSuccess) {
      toggleForgotPasswordSuccess()
    }
  }, [forgotPasswordSuccess, toggleForgotPasswordSuccess])

  useEffect(() => {
    if (error) {
      setTimeout(() => {
        setError(undefined)
      }, 5000)
    }
  }, [error])

  return (
    <ForgotPassword
      isLoading={loading}
      isValidEmail={isValidEmail}
      error={error}
      email={email}
      onSubmit={onForgotPasswordSubmit}
      onInputChanged={onInputChanged}
      onBackToLogin={resetForgotPassword}
      forgotPasswordSuccess={forgotPasswordSuccess}
    />
  )
}

export default memo(ForgotPasswordContainer)
