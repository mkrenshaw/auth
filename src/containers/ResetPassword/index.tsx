import React, { FC, memo, useCallback, useEffect, useReducer } from 'react'
import { parse } from 'querystringify'
import ResetPassword from 'src/components/ResetPassword'
import { useHistory, useLocation } from 'react-router-dom'
import { useFetch } from 'src/hooks'
import { InputOnChangeData } from 'semantic-ui-react/dist/commonjs/elements/Input/Input'
import {
  resetPassword as resetPasswordURL,
  verifyResetPassword as verifyResetPasswordURL
} from 'src/API'
import { validatePassword } from 'src/utils/password'

const ResetPasswordContainer: FC = () => {
  const { push } = useHistory()
  const { search } = useLocation()
  const [state, dispatch] = useReducer(resetPasswordReducer, {
    email: '',
    resetCode: '',
    password: '',
    passwordConfirm: '',
    passwordsMatch: true,
    resetPasswordSuccess: false,
    error: undefined,
    loading: false,
    passwordValid: true
  })
  const { fetchData: resetPassword } = useFetch<boolean>({
    url: resetPasswordURL,
    lazy: true,
    options: {
      method: 'POST'
    },
    onError: () =>
      dispatch({
        type: DispatchType.Error,
        payload: 'There was an issue resetting your password. Please try again.'
      }),
    onLoading: loading =>
      dispatch({ type: DispatchType.Loading, payload: loading }),
    onCompleted: success =>
      success &&
      dispatch({ type: DispatchType.ResetPasswordSuccess, payload: true })
  })
  const { fetchData: verifyResetPassword } = useFetch<boolean>({
    url: verifyResetPasswordURL,
    lazy: true,
    options: {
      method: 'POST'
    },
    onError: () =>
      dispatch({
        type: DispatchType.Error,
        payload:
          'There was an issue verifying your reset code. Please try again.'
      }),
    onLoading: loading =>
      dispatch({ type: DispatchType.Loading, payload: loading })
  })

  const onInputChanged = useCallback(
    (
      { currentTarget: { name } }: React.FormEvent<HTMLInputElement>,
      { value }: InputOnChangeData
    ) => {
      dispatch({
        type: DispatchType.InputChange,
        payload: {
          name,
          value
        }
      })
    },
    []
  )

  const onSubmit = useCallback(() => {
    const { resetCode, password, passwordConfirm, email } = state
    resetPassword({
      data: { resetCode, password, passwordConfirm, email }
    })
  }, [resetPassword, state])

  useEffect(() => {
    const resetParameters: Record<string, string> = parse(search)
    if (
      !resetParameters ||
      !resetParameters.email ||
      !resetParameters.resetCode
    ) {
      push('/')
      return
    }

    dispatch({ type: DispatchType.ResetParams, payload: resetParameters })

    verifyResetPassword({
      data: { email: state.email, resetCode: state.resetCode }
    })
  }, [push, search, state.email, state.resetCode, verifyResetPassword])

  useEffect(() => {
    if (state.error) {
      setTimeout(() => {
        dispatch({ type: DispatchType.Error, payload: undefined })
      }, 5000)
    }
  }, [state.error])

  return (
    <ResetPassword
      {...state}
      onSubmit={onSubmit}
      onInputChanged={onInputChanged}
    />
  )
}

const resetPasswordReducer = (
  state: State,
  actions: ResetPasswordActions
): State => {
  if (actions.type === DispatchType.InputChange) {
    const key = actions.payload.name
    const value = actions.payload.value

    return {
      ...state,
      [key]: value,
      ...(key === 'password' && {
        passwordsMatch:
          state.passwordConfirm === value && state.password.length > 0,
        passwordValid: value.length > 0 && validatePassword(value)
      }),
      ...(key === 'passwordConfirm' && {
        passwordsMatch: state.password === value && state.password.length > 0
      })
    }
  }

  if (actions.type === DispatchType.ResetPasswordSuccess) {
    return {
      ...state,
      resetPasswordSuccess: actions.payload
    }
  }

  if (actions.type === DispatchType.ResetParams) {
    return {
      ...state,
      email: actions.payload.email,
      resetCode: actions.payload.resetCode
    }
  }

  if (actions.type === DispatchType.Loading) {
    return {
      ...state,
      loading: actions.payload
    }
  }

  if (actions.type === DispatchType.Error) {
    return {
      ...state,
      error: actions.payload
    }
  }

  return state
}

interface State {
  email: string
  resetCode: string
  password: string
  passwordConfirm: string
  passwordsMatch: boolean
  passwordValid: boolean
  resetPasswordSuccess: boolean
  error?: string
  loading?: boolean
}

enum DispatchType {
  ResetPasswordSuccess,
  ResetParams,
  InputChange,
  Loading,
  Error
}

interface InputChangeAction {
  type: DispatchType.InputChange
  payload: {
    name: string
    value: string
  }
}

interface LoadingAction {
  type: DispatchType.Loading
  payload?: boolean
}

interface ErrorAction {
  type: DispatchType.Error
  payload?: string
}

interface ResetParamsAction {
  type: DispatchType.ResetParams
  payload: Record<string, string>
}

interface ResetPasswordSuccessAction {
  type: DispatchType.ResetPasswordSuccess
  payload: boolean
}

type ResetPasswordActions =
  | InputChangeAction
  | LoadingAction
  | ErrorAction
  | ResetParamsAction
  | ResetPasswordSuccessAction

export default memo(ResetPasswordContainer)
