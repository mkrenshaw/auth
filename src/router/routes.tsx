import { lazy } from 'react'
import { RouteProps } from 'react-router-dom'

// Unauthenticated
const LoginPage = lazy(() => import('src/pages/Login'))
const ForgotPasswordPage = lazy(() => import('src/pages/ForgotPassword'))
const ResetPasswordPage = lazy(() => import('src/pages/ResetPassword'))

// Authenticated
const OverviewPage = lazy(() => import('src/pages/Overview'))

export const unauthenticatedRoutes: RouteProps[] = [
  {
    path: '/',
    component: LoginPage,
    exact: true
  },
  {
    path: '/forgot-password',
    component: ForgotPasswordPage,
    exact: true
  },
  {
    path: '/reset-password',
    component: ResetPasswordPage,
    exact: true
  }
]

export const authenticatedRoutes: RouteProps[] = [
  {
    path: '/',
    component: OverviewPage,
    exact: true
  }
]
