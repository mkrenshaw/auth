import React, { FC, lazy, memo } from 'react'
import createRoutes from 'src/utils/createRoutes'
import { unauthenticatedRoutes } from './routes'

const UnauthenticatedAppWrapper = lazy(
  () => import('src/components/App/UnauthenticatedApp')
)

const UnauthenticatedRouter: FC = () => (
  <UnauthenticatedAppWrapper>
    {createRoutes(unauthenticatedRoutes)}
  </UnauthenticatedAppWrapper>
)

export default memo(UnauthenticatedRouter)
