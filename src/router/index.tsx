import React, { FC, lazy } from 'react'
import { useAuth } from 'src/hooks'
import { Loader } from '@120wateraudit/envirio-components/dist/components/Loader'

const AuthenticatedRouter = lazy(() => import('./AuthenticatedRouter'))
const UnauthenticatedRouter = lazy(() => import('./UnauthenticatedRouter'))

const Router: FC = () => {
  const { authenticated, authLoading } = useAuth()

  if (authLoading) {
    return <Loader />
  }

  return authenticated ? <AuthenticatedRouter /> : <UnauthenticatedRouter />
}

export default Router
