import React, { FC, lazy, memo } from 'react'
import createRoutes from 'src/utils/createRoutes'
import { authenticatedRoutes } from './routes'

const AppWrapper = lazy(() => import('src/components/App'))

const AuthenticatedRouter: FC = () => (
  <AppWrapper>{createRoutes(authenticatedRoutes)}</AppWrapper>
)

export default memo(AuthenticatedRouter)
