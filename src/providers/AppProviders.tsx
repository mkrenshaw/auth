import { User } from '@120wateraudit/envirio-components/dist/models'
import React, { FC, memo, ReactNode } from 'react'
import UserProvider from './UserProvider'

const AppProviders: FC<Props> = ({ children, user }) => (
  <UserProvider user={user}>{children}</UserProvider>
)

interface Props {
  children: ReactNode
  user: User
}

export default memo(AppProviders)
