import React, { useMemo, FC, ReactNode, memo } from 'react'
import { UserContext } from 'src/contexts'
import { User } from '@120wateraudit/envirio-components/dist/models'

const UserProvider: FC<Props> = memo(props => {
  const { user } = props
  const value = useMemo(() => user, [user])

  return <UserContext.Provider value={value} {...props} />
})

interface Props {
  children: ReactNode
  user: User
}

export default memo(UserProvider)
