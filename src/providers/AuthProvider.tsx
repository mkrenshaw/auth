import React, {
  memo,
  FC,
  useCallback,
  useState,
  useEffect,
  ReactNode,
  useMemo
} from 'react'
import { AuthContext } from 'src/contexts/AuthContext'
import { useFetch } from 'src/hooks'
import { useHistory, useLocation } from 'react-router-dom'
import { login as loginURL, getCurrentUser, validateToken } from 'src/API'
import { User } from '@120wateraudit/envirio-components/dist/models'
import { unauthenticatedRoutes } from '../router/routes'
import {
  setItem,
  LocalStorageItem,
  clearItems,
  getItem
} from 'src/utils/localStorage'

const AuthProvider: FC<Props> = props => {
  const [loginLoading, setLoadingLoading] = useState<boolean | undefined>(false)
  const [authLoading, setAuthLoading] = useState<boolean | undefined>(true)
  const [error, setError] = useState<string | undefined>(undefined)
  const [user, setUser] = useState<User | undefined>(undefined)
  const [token, setToken] = useState<string | undefined>(undefined)
  const [authenticated, setAuthenticated] = useState<boolean>(false)
  const { push } = useHistory()
  const { pathname } = useLocation()

  const { fetchData: loginUser } = useFetch<{
    token: string
  }>({
    url: loginURL,
    lazy: true,
    options: {
      method: 'POST'
    }
  })
  const { fetchData: getUser } = useFetch<User>({
    url: getCurrentUser,
    lazy: true
  })
  const { fetchData: checkToken } = useFetch<boolean>({
    url: validateToken,
    lazy: true,
    options: {
      method: 'POST'
    }
  })

  const login = useCallback(
    async ({ username, password }: { username: string; password: string }) => {
      setLoadingLoading(true)

      if (username && password) {
        try {
          const response = await loginUser({
            data: { username, password }
          })

          if (response && response.token) {
            setItem(LocalStorageItem.TOKEN, response.token)
            setToken(response.token)
          }

          const userResponse = await getUser()

          if (userResponse) {
            setItem(LocalStorageItem.USER, userResponse)
            setUser(userResponse)
          }

          if (response && response.token && userResponse) {
            setAuthenticated(true)
            push('/')
          }
        } catch (error) {
          let message = error.message
          if (
            error.response &&
            error.response.status &&
            error.response.status === 401
          ) {
            message = 'Incorrect username or password.'
          } else if (
            error.response &&
            error.response.status &&
            error.response.status === 403
          ) {
            message = '403'
          } else if (
            error.response &&
            error.response.data &&
            error.response.data.message
          ) {
            message = error.response.data.message
          }
          setError(message)
        } finally {
          setLoadingLoading(false)
        }
      }
    },
    [loginUser, getUser, push]
  )

  const handleUnauthenticated = useCallback(() => {
    clearItems()
    setAuthenticated(false)

    if (
      !unauthenticatedRoutes.map(r => r.path).some(path => path === pathname)
    ) {
      push('/')
    }
  }, [push, pathname])

  const logout = useCallback(() => {
    handleUnauthenticated()
  }, [handleUnauthenticated])

  const values = useMemo(
    () => ({
      login,
      logout,
      loginLoading,
      authenticated,
      authLoading,
      error,
      ...(user &&
        token && {
          data: {
            user,
            token
          }
        })
    }),
    [
      logout,
      login,
      loginLoading,
      authenticated,
      authLoading,
      error,
      user,
      token
    ]
  )

  useEffect(() => {
    if (error) {
      setTimeout(() => {
        setError(undefined)
      }, 5000)
    }
  }, [error])

  useEffect(() => {
    async function authenticate() {
      setAuthLoading(true)
      try {
        const localStorageUser = getItem(LocalStorageItem.USER)
        const localStorageToken = getItem(LocalStorageItem.TOKEN)

        if (
          !localStorageUser ||
          typeof localStorageUser !== 'string' ||
          !localStorageToken
        ) {
          handleUnauthenticated()
          return
        }

        const isValid = await checkToken({
          data: { token: localStorageToken }
        })

        if (!isValid) {
          handleUnauthenticated()
          return
        }

        setUser(JSON.parse(localStorageUser))
        setToken(localStorageToken)
        setAuthenticated(true)
      } catch (e) {
        setError('Session Expired')
        handleUnauthenticated()
      } finally {
        setAuthLoading(false)
      }
    }
    authenticate()
  }, [checkToken, handleUnauthenticated])

  return <AuthContext.Provider value={values} {...props} />
}

interface Props {
  children: ReactNode
}

export default memo(AuthProvider)
