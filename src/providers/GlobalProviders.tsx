import React, { FC, Suspense, memo, ReactNode } from 'react'
import ThemeProvider from './ThemeProvider'
import AuthProvider from './AuthProvider'
import { Loader } from '@120wateraudit/envirio-components/dist/components/Loader'
import { BrowserRouter } from 'react-router-dom'

const GlobalProviders: FC<Props> = ({ children }) => (
  <Suspense fallback={<Loader />}>
    <ThemeProvider>
      <BrowserRouter>
        <AuthProvider>{children}</AuthProvider>
      </BrowserRouter>
    </ThemeProvider>
  </Suspense>
)

interface Props {
  children: ReactNode
}

export default memo(GlobalProviders)
